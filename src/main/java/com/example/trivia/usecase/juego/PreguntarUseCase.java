package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import com.example.trivia.domain.juego.Juego;
import com.example.trivia.domain.juego.Pregunta;
import com.example.trivia.domain.juego.command.Preguntar;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;

import java.util.HashMap;
import java.util.Map;

public class PreguntarUseCase extends UseCase<RequestCommand<Preguntar>, ResponseEvents> {

    @Override
    public void executeUseCase(RequestCommand<Preguntar> preguntarRequestCommand) {
        var command = preguntarRequestCommand.getCommand();

        var juego = Juego.from(command.getJuegoId(), retrieveEvents());

        Map<PreguntaId, Pregunta> preguntas = new HashMap<>();

        var preguntaId = command.getPreguntas().keySet();

        preguntaId.forEach(id -> {
            preguntas.put(id,new Pregunta(id,command.getPreguntas().get(id).value(),command.getPreguntas().get(id).respuesta()));
        });

        juego.adicionarPreguntas(command.getJuegoId(), preguntas);

        emit().onResponse(new ResponseEvents(juego.getUncommittedChanges()));

    }
}
