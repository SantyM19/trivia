package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import com.example.trivia.domain.juego.Juego;
import com.example.trivia.domain.juego.Jugador;
import com.example.trivia.domain.juego.command.SeleccionarGanadores;
import com.example.trivia.domain.juego.values.JugadorId;

import java.util.HashMap;
import java.util.Map;

public class SeleccionarGanadoresUseCase extends UseCase<RequestCommand<SeleccionarGanadores>, ResponseEvents> {

    @Override
    public void executeUseCase(RequestCommand<SeleccionarGanadores> seleccionarGanadoresRequestCommand) {
        var command = seleccionarGanadoresRequestCommand.getCommand();

        var juego = Juego.from(command.getJuegoId(), retrieveEvents());

        Map<JugadorId, Jugador> ganadores = new HashMap<>();

        var jugadorId = command.getJugadores().keySet();
        var jugadores = command.getJugadores();

        Ganador(jugadores);

        jugadorId.forEach(id -> {
            ganadores.put(id,new Jugador(
                    id,
                    command.getJugadores().get(id).nombre(),
                    command.getJugadores().get(id).puntos(),
                    command.getJugadores().get(id).ganadas()
            ));
        });


        juego.seleccionGanadores(command.getJuegoId(), ganadores);

        emit().onResponse(new ResponseEvents(juego.getUncommittedChanges()));

    }

    private void Ganador(Map<JugadorId, Jugador> jugadores) {
        int puntoMax = 0;
        JugadorId idGanador = JugadorId.of("0");

        for(JugadorId i : jugadores.keySet()){
            if(jugadores.get(i).puntos().value()>puntoMax){
                puntoMax = jugadores.get(i).puntos().value();
                idGanador = i;
            }
        }

        jugadores.get(idGanador).aumentarGanadas(1);
    }

}
