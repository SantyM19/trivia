package com.example.trivia.usecase.juego;


import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import com.example.trivia.domain.juego.Juego;
import com.example.trivia.domain.juego.command.InicializarJuego;

public class InicializarJuegoUseCase extends UseCase<RequestCommand<InicializarJuego>, ResponseEvents>{
    @Override
    public void executeUseCase(RequestCommand<InicializarJuego> input) {
        var command = input.getCommand();

        var juego = Juego.from(command.getJuegoId(), retrieveEvents());

        juego.iniciarJuego();

        emit().onResponse(new ResponseEvents(juego.getUncommittedChanges()));

    }


}
