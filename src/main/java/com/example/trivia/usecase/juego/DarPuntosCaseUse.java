package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import com.example.trivia.domain.juego.Juego;
import com.example.trivia.domain.juego.Jugador;
import com.example.trivia.domain.juego.Pregunta;
import com.example.trivia.domain.juego.command.DarPuntos;
import com.example.trivia.domain.juego.values.JugadorId;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;
import com.example.trivia.domain.juego.values.pregunta.Respuesta;


import java.util.HashMap;
import java.util.Map;

public class DarPuntosCaseUse extends UseCase<RequestCommand<DarPuntos>, ResponseEvents> {

    @Override
    public void executeUseCase(RequestCommand<DarPuntos> darPuntosRequestCommand) {
        var command = darPuntosRequestCommand.getCommand();

        var juego = Juego.from(command.getJuegoId(), retrieveEvents());

        Map<JugadorId, Jugador> puntos = new HashMap<>();

        var jugadorId = command.getJugador().keySet();


        jugadorId.forEach(id -> {
            Puntuar(command.getPreguntas(),command.getRespuestas(),command.getJugador(),id);
            puntos.put(id,new Jugador(id,command.getJugador().get(id).nombre(), command.getJugador().get(id).puntos(), command.getJugador().get(id).ganadas()));
        });

        juego.darPuntos(command.getJuegoId(), puntos);

        emit().onResponse(new ResponseEvents(juego.getUncommittedChanges()));

    }

    private void Puntuar(Map<PreguntaId, Pregunta> preguntas, Map<PreguntaId, Respuesta> respuestas, Map<JugadorId, Jugador> puntos, JugadorId id) {
        var pId = preguntas.keySet();

        pId.forEach((pid) -> {
            if(preguntas.get(pid).respuesta().getRespuesta() == respuestas.get(pid).getRespuesta()){
                System.out.println(":v");
                puntos.get(id).aumentarPuntos(1); }
        });

    }


}
