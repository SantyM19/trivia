package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.UseCase;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.business.support.ResponseEvents;
import com.example.trivia.domain.juego.Juego;
import com.example.trivia.domain.juego.RespuestaEntidad;
import com.example.trivia.domain.juego.command.RecibirRespuestas;
import com.example.trivia.domain.juego.values.pregunta.RespuestaId;

import java.util.HashMap;
import java.util.Map;

public class RecibirRespuestasUseCase extends UseCase<RequestCommand<RecibirRespuestas>, ResponseEvents> {
    @Override
    public void executeUseCase(RequestCommand<RecibirRespuestas> recibirRespuestasRequestCommand) {
        var command = recibirRespuestasRequestCommand.getCommand();

        var juego = Juego.from(command.getJuegoId(), retrieveEvents());

        Map<RespuestaId, RespuestaEntidad> respuestas = new HashMap<>();

        var respuestaId = command.getRespuestas().keySet();

        respuestaId.forEach(rId -> {
            respuestas.put(rId,new RespuestaEntidad(rId,command.getJugadorId(),command.getJuegoId(),command.getPreguntas().get(rId),command.getRespuestas().get(rId)));
        });

        juego.adicionarRespuestas(command.getJuegoId(), respuestas);

        emit().onResponse(new ResponseEvents(juego.getUncommittedChanges()));

    }
}
