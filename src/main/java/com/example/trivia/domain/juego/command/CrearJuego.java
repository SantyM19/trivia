package com.example.trivia.domain.juego.command;

import co.com.sofka.domain.generic.Command;
import com.example.trivia.domain.juego.values.*;

import java.util.Map;

public class CrearJuego implements Command {
    private final Map<JugadorId, Ganadas>ganadas;
    private final Map<JugadorId, Puntos> puntos;
    private final Map<JugadorId, Nombre> nombres;
    private final JuegoId juegoId;

    public CrearJuego(Map<JugadorId, Ganadas> ganadas, Map<JugadorId, Puntos> puntos, Map<JugadorId, Nombre> nombres, JuegoId juegoId) {
        this.ganadas = ganadas;
        this.puntos = puntos;
        this.nombres = nombres;
        this.juegoId = juegoId;
    }

    public Map<JugadorId, Puntos> getPuntos() {
        return puntos;
    }

    public Map<JugadorId, Nombre> getNombres() {
        return nombres;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public Map<JugadorId, Ganadas> getGanadas() {
        return ganadas;
    }
}