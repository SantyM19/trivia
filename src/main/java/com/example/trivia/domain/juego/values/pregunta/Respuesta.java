package com.example.trivia.domain.juego.values.pregunta;

import co.com.sofka.domain.generic.ValueObject;

public class Respuesta implements ValueObject<Boolean> {
    private final Boolean respuesta;


    public Respuesta(Boolean respuesta) {
        this.respuesta = respuesta;
    }

    public Boolean getRespuesta() {
        return respuesta;
    }

    @Override
    public Boolean value() {
        return null;
    }
}
