package com.example.trivia.domain.juego.events;

import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.Pregunta;
import com.example.trivia.domain.juego.values.JuegoId;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;

import java.util.Map;

public class PreguntasRealizadas extends DomainEvent {
    private final JuegoId juegoId;
    private final Map<PreguntaId, Pregunta> preguntas;

    public PreguntasRealizadas(JuegoId juegoId, Map<PreguntaId, Pregunta> preguntas) {
        super("trivia.juego.PreguntasRealizadas");
        this.juegoId = juegoId;
        this.preguntas = preguntas;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public Map<PreguntaId, Pregunta> getPreguntas() {
        return preguntas;
    }
}
