package com.example.trivia.domain.juego.values;

import co.com.sofka.domain.generic.ValueObject;

import java.util.Objects;

public class Puntos implements ValueObject<Integer> {
    private final Integer value;

    public Puntos(Integer value) {
        this.value = Objects.requireNonNull(value);
        if (value < 0) {
            throw new IllegalArgumentException("El valor de los puntos no puede ser negativo");
        }
    }


    public Puntos aumentar(Integer value) {
        return new Puntos(this.value + value);
    }

    public Puntos disminuir(Integer value) {
        return new Puntos(this.value - value);
    }

    @Override
    public Integer value() {
        return value;
    }
}