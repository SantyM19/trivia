package com.example.trivia.domain.juego.factory;

import com.example.trivia.domain.juego.Jugador;
import com.example.trivia.domain.juego.values.Ganadas;
import com.example.trivia.domain.juego.values.JugadorId;
import com.example.trivia.domain.juego.values.Nombre;
import com.example.trivia.domain.juego.values.Puntos;

import java.util.HashSet;
import java.util.Set;

public class JugadorFactory {
    private final Set<Jugador> jugadores;

    private JugadorFactory() {
        jugadores = new HashSet<>();
    }

    public static JugadorFactory builder() {
        return new JugadorFactory();
    }

    public void nuevoJugador(JugadorId jugadorId, Nombre nombre, Puntos puntos, Ganadas ganadas) {
        jugadores.add(new Jugador(jugadorId, nombre, puntos, ganadas));
    }

    public Set<Jugador> jugadores() {
        return jugadores;
    }
}