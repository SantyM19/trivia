package com.example.trivia.domain.juego.events;

import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.Jugador;
import com.example.trivia.domain.juego.values.JuegoId;
import com.example.trivia.domain.juego.values.JugadorId;

import java.util.Map;

public class PuntuacionesDadas extends DomainEvent {
    private final JuegoId juegoId;
    private final Map<JugadorId, Jugador> jugadorPuntos;

    public PuntuacionesDadas(JuegoId juegoId, Map<JugadorId, Jugador> respuestas) {
        super("trivia.juego.PuntuacionesDadas");
        this.juegoId = juegoId;
        this.jugadorPuntos = respuestas;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public Map<JugadorId, Jugador> getJugadorPuntos() {
        return jugadorPuntos;
    }
}
