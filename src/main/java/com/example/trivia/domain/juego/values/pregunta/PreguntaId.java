package com.example.trivia.domain.juego.values.pregunta;

import co.com.sofka.domain.generic.Identity;

public class PreguntaId extends Identity {
    private PreguntaId(String uid) {
        super(uid);
    }

    public PreguntaId() {
    }

    public static PreguntaId of(String uid) {
        return new PreguntaId(uid);
    }
}
