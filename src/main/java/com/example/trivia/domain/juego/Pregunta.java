package com.example.trivia.domain.juego;

import co.com.sofka.domain.generic.Entity;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;
import com.example.trivia.domain.juego.values.pregunta.Respuesta;

import java.util.Objects;

public class Pregunta extends Entity<PreguntaId> {
    private final String value;
    private final Respuesta respuesta;

    public Pregunta(PreguntaId entityId, String value, Respuesta respuesta) {
        super(entityId);
        this.value = Objects.requireNonNull(value, "El valor de la pregunta es requerida");
        this.respuesta = respuesta;
    }

    public String value() {
        return value;
    }

    public Respuesta respuesta(){
        return respuesta;
    }

}
