package com.example.trivia.domain.juego.command;

import co.com.sofka.domain.generic.Command;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;
import com.example.trivia.domain.juego.values.pregunta.Respuesta;
import com.example.trivia.domain.juego.values.JuegoId;
import com.example.trivia.domain.juego.values.JugadorId;
import com.example.trivia.domain.juego.values.pregunta.RespuestaId;

import java.util.Map;

public class RecibirRespuestas implements Command {
    private final JuegoId juegoId;
    private final JugadorId jugadorId;
    private final Map<RespuestaId, Respuesta> respuestas;
    private final Map<RespuestaId, PreguntaId> preguntas;

    public RecibirRespuestas(JuegoId juegoId, JugadorId jugadorId, Map<RespuestaId, Respuesta> respuestas, Map<RespuestaId, PreguntaId> preguntas) {
        this.juegoId = juegoId;
        this.jugadorId = jugadorId;
        this.respuestas = respuestas;
        this.preguntas = preguntas;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public JugadorId getJugadorId() {
        return jugadorId;
    }

    public Map<RespuestaId, Respuesta> getRespuestas() {
        return respuestas;
    }

    public Map<RespuestaId, PreguntaId> getPreguntas() {
        return preguntas;
    }
}
