package com.example.trivia.domain.juego;

import co.com.sofka.domain.generic.AggregateEvent;
import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.events.*;
import com.example.trivia.domain.juego.factory.JugadorFactory;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;
import com.example.trivia.domain.juego.values.pregunta.RespuestaId;
import com.example.trivia.domain.juego.values.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Juego extends AggregateEvent<JuegoId> {

    protected Boolean juegoInicializado;
    protected Map<JugadorId, Jugador> jugadores;
    protected Map<RespuestaId, RespuestaEntidad> respuestas;
    protected Map<PreguntaId, Pregunta> preguntas;
    protected Map<JugadorId, Jugador> puntos;
    protected Map<JugadorId, Jugador> ganador;


    public Juego(JuegoId entityId, JugadorFactory jugadorFactory) {
        super(entityId);
        respuestas = new HashMap<>();
        appendChange(new JuegoCreado(entityId)).apply();
        jugadorFactory.jugadores()
                .forEach(jugador -> adicionarJugador(jugador.identity(), jugador.nombre(), jugador.puntos(), jugador.ganadas()));
    }

    private Juego(JuegoId entityId) {
        super(entityId);
        subscribe(new JuegoChange(this));
    }

    public static Juego from(JuegoId entityId, List<DomainEvent> events) {
        var juego = new Juego(entityId);
        events.forEach(juego::applyEvent);
        return juego;
    }

    public void adicionarJugador(JugadorId jugadorId, Nombre nombre, Puntos puntos, Ganadas ganadas) {
        appendChange(new JugadorAdicionado(jugadorId, nombre, puntos, ganadas)).apply();
    }

    public void iniciarJuego() {
        var jugadoresIds = jugadores.keySet();
        appendChange(new JuegoInicializado(jugadoresIds)).apply();
    }

    public void adicionarPreguntas(JuegoId juegoId,Map<PreguntaId, Pregunta>  preguntas){
        appendChange(new PreguntasRealizadas(juegoId, preguntas)).apply();
    }

    public void adicionarRespuestas(JuegoId juegoId,Map<RespuestaId, RespuestaEntidad>  respuestas){
        appendChange(new RespuestasRecibidas(juegoId, respuestas)).apply();
    }

    public void darPuntos(JuegoId juegoId,Map<JugadorId, Jugador> puntos){
        appendChange(new PuntuacionesDadas(juegoId, puntos)).apply();
    }

    public void seleccionGanadores(JuegoId juegoId,Map<JugadorId, Jugador> ganadores){
        appendChange(new GanadoresSeleccionados(juegoId, ganadores)).apply();
    }



}