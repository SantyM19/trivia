package com.example.trivia.domain.juego.command;

import co.com.sofka.domain.generic.Command;
import com.example.trivia.domain.juego.Jugador;
import com.example.trivia.domain.juego.values.JuegoId;
import com.example.trivia.domain.juego.values.JugadorId;

import java.util.Map;

public class SeleccionarGanadores implements Command {
    private final JuegoId juegoId;
    private final Map<JugadorId, Jugador> jugadores;

    public SeleccionarGanadores(JuegoId juegoId, Map<JugadorId, Jugador> jugadores) {
        this.juegoId = juegoId;
        this.jugadores = jugadores;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public Map<JugadorId, Jugador> getJugadores() {
        return jugadores;
    }
}
