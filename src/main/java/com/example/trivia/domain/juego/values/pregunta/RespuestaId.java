package com.example.trivia.domain.juego.values.pregunta;

import co.com.sofka.domain.generic.Identity;

public class RespuestaId extends Identity {
    private RespuestaId(String uid) {
        super(uid);
    }

    public RespuestaId() {
    }

    public static RespuestaId of(String uid) {
        return new RespuestaId(uid);
    }
}
