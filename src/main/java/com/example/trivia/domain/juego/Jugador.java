package com.example.trivia.domain.juego;

import co.com.sofka.domain.generic.Entity;
import com.example.trivia.domain.juego.values.Ganadas;
import com.example.trivia.domain.juego.values.Puntos;
import com.example.trivia.domain.juego.values.JugadorId;
import com.example.trivia.domain.juego.values.Nombre;

public class Jugador extends Entity<JugadorId> {

    private final Nombre nombre;
    private Puntos puntos;
    private Ganadas ganadas;

    public Jugador(JugadorId entityId, Nombre nombre, Puntos puntos, Ganadas ganadas) {
        super(entityId);
        this.nombre = nombre;
        this.puntos = puntos;
        this.ganadas = ganadas;
    }

    public void aumentarPuntos(Integer valor) {
        this.puntos = this.puntos.aumentar(valor);
    }

    public void aumentarGanadas(Integer valor) {
        this.ganadas = this.ganadas.aumentar(valor);
    }

    public Puntos puntos() {
        return puntos;
    }

    public Nombre nombre() {
        return nombre;
    }

    public Ganadas ganadas() {
        return ganadas;
    }

}
