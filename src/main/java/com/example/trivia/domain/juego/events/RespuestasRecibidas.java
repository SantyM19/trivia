package com.example.trivia.domain.juego.events;

import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.RespuestaEntidad;
import com.example.trivia.domain.juego.values.JuegoId;
import com.example.trivia.domain.juego.values.pregunta.RespuestaId;

import java.util.Map;

public class RespuestasRecibidas extends DomainEvent {
    private final JuegoId juegoId;
    private final Map<RespuestaId, RespuestaEntidad> respuestas;

    public RespuestasRecibidas(JuegoId jeugoId, Map<RespuestaId, RespuestaEntidad> respuestas) {
        super("trivia.juego.RespuestasRecibidas");
        this.juegoId = jeugoId;
        this.respuestas = respuestas;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public Map<RespuestaId, RespuestaEntidad> getRespuestas() {
        return respuestas;
    }
}
