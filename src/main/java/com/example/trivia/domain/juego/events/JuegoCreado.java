package com.example.trivia.domain.juego.events;

import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.values.JuegoId;

public class JuegoCreado extends DomainEvent {
    private final JuegoId juegoId;

    public JuegoCreado(JuegoId juegoId) {
        super("trivia.juego.creado");
        this.juegoId = juegoId;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }
}
