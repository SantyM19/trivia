package com.example.trivia.domain.juego.values;

import co.com.sofka.domain.generic.ValueObject;

import java.util.Objects;

public class Ganadas implements ValueObject<Integer> {
    private final Integer value;

    public Ganadas(Integer value) {
        this.value = Objects.requireNonNull(value);
    }


    public Ganadas aumentar(Integer value) {
        return new Ganadas(this.value + value);
    }

    public Ganadas disminuir(Integer value) {
        return new Ganadas(this.value - value);
    }

    @Override
    public Integer value() {
        return value;
    }

}
