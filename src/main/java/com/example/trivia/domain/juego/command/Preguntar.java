package com.example.trivia.domain.juego.command;

import co.com.sofka.domain.generic.Command;
import com.example.trivia.domain.juego.Pregunta;
import com.example.trivia.domain.juego.values.JuegoId;
import com.example.trivia.domain.juego.values.JugadorId;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;

import java.util.Map;

public class Preguntar implements Command {
    private final JuegoId juegoId;
    private final JugadorId jugadorId;
    private final Map<PreguntaId, Pregunta> preguntas;

    public Preguntar(JuegoId juegoId, JugadorId jugadorId, Map<PreguntaId, Pregunta> preguntas) {
        this.juegoId = juegoId;
        this.jugadorId = jugadorId;
        this.preguntas = preguntas;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public JugadorId getJugadorId() {
        return jugadorId;
    }

    public Map<PreguntaId, Pregunta> getPreguntas() {
        return preguntas;
    }
}
