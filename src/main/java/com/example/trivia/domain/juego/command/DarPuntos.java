package com.example.trivia.domain.juego.command;

import co.com.sofka.domain.generic.Command;
import com.example.trivia.domain.juego.Jugador;
import com.example.trivia.domain.juego.Pregunta;
import com.example.trivia.domain.juego.values.JuegoId;
import com.example.trivia.domain.juego.values.JugadorId;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;
import com.example.trivia.domain.juego.values.pregunta.Respuesta;

import java.util.Map;

public class DarPuntos implements Command {
    private final JuegoId juegoId;
    private final Map<JugadorId,Jugador> jugador;
    private final Map<PreguntaId, Pregunta> preguntas;
    private final Map<PreguntaId, Respuesta> respuestas;

    public DarPuntos(JuegoId juegoId, Map<JugadorId,Jugador> jugador, Map<PreguntaId, Pregunta> preguntas, Map<PreguntaId, Respuesta> respuestas) {
        this.juegoId = juegoId;
        this.jugador = jugador;
        this.preguntas = preguntas;
        this.respuestas = respuestas;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public Map<JugadorId,Jugador> getJugador() {
        return jugador;
    }

    public Map<PreguntaId, Pregunta> getPreguntas() {
        return preguntas;
    }

    public Map<PreguntaId, Respuesta> getRespuestas() {
        return respuestas;
    }
}
