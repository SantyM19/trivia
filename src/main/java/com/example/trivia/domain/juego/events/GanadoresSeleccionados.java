package com.example.trivia.domain.juego.events;

import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.Jugador;
import com.example.trivia.domain.juego.values.JuegoId;
import com.example.trivia.domain.juego.values.JugadorId;

import java.util.Map;

public class GanadoresSeleccionados extends DomainEvent {
    private final JuegoId juegoId;
    private final Map<JugadorId, Jugador> ganador;

    public GanadoresSeleccionados(JuegoId juegoId, Map<JugadorId, Jugador> ganador) {
        super("trivia.juego.GanadoresSeleccionados");
        this.juegoId = juegoId;
        this.ganador = ganador;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public Map<JugadorId, Jugador> getGanador() { return ganador; }
}
