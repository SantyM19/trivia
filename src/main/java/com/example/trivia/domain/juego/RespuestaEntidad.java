package com.example.trivia.domain.juego;

import co.com.sofka.domain.generic.Entity;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;
import com.example.trivia.domain.juego.values.pregunta.Respuesta;
import com.example.trivia.domain.juego.values.JuegoId;
import com.example.trivia.domain.juego.values.JugadorId;
import com.example.trivia.domain.juego.values.pregunta.RespuestaId;

public class RespuestaEntidad extends Entity<RespuestaId> {

    private final JugadorId jugadorId;
    private final JuegoId juegoId;
    private final PreguntaId preguntaId;
    private final Respuesta respuesta;

    public RespuestaEntidad(RespuestaId entityId ,JugadorId jugadorId, JuegoId juegoId, PreguntaId preguntaId, Respuesta respuesta) {
        super(entityId);
        this.jugadorId = jugadorId;
        this.juegoId = juegoId;
        this.preguntaId = preguntaId;
        this.respuesta = respuesta;
    }

    public JugadorId getJugadorId() {
        return jugadorId;
    }

    public JuegoId getJuegoId() {
        return juegoId;
    }

    public PreguntaId getPreguntaId() {
        return preguntaId;
    }

    public Respuesta getRespuesta() {
        return respuesta;
    }
}
