package com.example.trivia.domain.juego.events;

import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.values.JugadorId;

import java.util.Set;

public class JuegoInicializado extends DomainEvent {
    private final Set<JugadorId> jugadoresIds;

    public JuegoInicializado(Set<JugadorId> jugadoresIds) {
        super("trivia.juego.juego inicializado");
        this.jugadoresIds = jugadoresIds;
    }

    public Set<JugadorId> getJugadoresIds() {
        return jugadoresIds;
    }
}
