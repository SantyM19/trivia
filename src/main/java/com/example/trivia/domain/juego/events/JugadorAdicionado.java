package com.example.trivia.domain.juego.events;

import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.values.Ganadas;
import com.example.trivia.domain.juego.values.JugadorId;
import com.example.trivia.domain.juego.values.Nombre;
import com.example.trivia.domain.juego.values.Puntos;

public class JugadorAdicionado extends DomainEvent {
    private final JugadorId jugadorId;
    private final Nombre nombre;
    private final Puntos puntos;
    private final Ganadas ganadas;

    public JugadorAdicionado(JugadorId jugadorId, Nombre nombre, Puntos puntos, Ganadas ganadas) {
        super("trivia.juego.jugador adicionado");
        this.jugadorId = jugadorId;
        this.nombre = nombre;
        this.puntos = puntos;
        this.ganadas = ganadas;
    }

    public JugadorId getJugadorId() {
        return jugadorId;
    }

    public Puntos getPuntos() {
        return puntos;
    }

    public Nombre getNombre() {
        return nombre;
    }


    public Ganadas getGanadas() {
        return ganadas;
    }
}
