package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.BusinessException;
import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.support.RequestCommand;
import com.example.trivia.domain.juego.command.CrearJuego;
import com.example.trivia.domain.juego.events.JuegoCreado;
import com.example.trivia.domain.juego.events.JugadorAdicionado;
import com.example.trivia.domain.juego.values.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class CrearJuegoUseCaseTest {
    @Test
    void crearUnJuego() {
        var nombres = Map.of(
                JugadorId.of("0001"), new Nombre("Rigel"),
                JugadorId.of("0002"), new Nombre("Betelgeuse")
        );
        var puntos = Map.of(
                JugadorId.of("0001"), new Puntos(0),
                JugadorId.of("0002"), new Puntos(0)
        );

        var ganadas = Map.of(
                JugadorId.of("0001"), new Ganadas(0),
                JugadorId.of("0002"), new Ganadas(0)
        );
        var command = new CrearJuego(ganadas, puntos, nombres, new JuegoId());
        var useCase = new CrearJuegoUseCase();

        var events = UseCaseHandler.getInstance()
                .syncExecutor(useCase, new RequestCommand<>(command))
                .orElseThrow()
                .getDomainEvents();

        var juegoCreado = (JuegoCreado) events.get(0);
        var jugadorAdicionadoParaRigel = (JugadorAdicionado) events.get(2);
        var jugadorAdicionadoParaBetelgeuse = (JugadorAdicionado) events.get(1);

        assertTrue(Objects.nonNull(juegoCreado.getJuegoId().value()));

        assertEquals("Rigel", jugadorAdicionadoParaRigel.getNombre().value());
        assertEquals(0, jugadorAdicionadoParaRigel.getPuntos().value());
        assertEquals("0001", jugadorAdicionadoParaRigel.getJugadorId().value());

        assertEquals("Betelgeuse", jugadorAdicionadoParaBetelgeuse.getNombre().value());
        assertEquals(0, jugadorAdicionadoParaBetelgeuse.getPuntos().value());
        assertEquals("0002", jugadorAdicionadoParaBetelgeuse.getJugadorId().value());

    }


    @Test
    void errorAlCrearJuego() {
        var nombres = Map.of(
                JugadorId.of("0000"), new Nombre("Me")
        );
        var puntos = Map.of(
                JugadorId.of("0000"), new Puntos(0)
        );
        var ganadas = Map.of(
                JugadorId.of("0000"), new Ganadas(0)
        );
        var command = new CrearJuego(ganadas, puntos, nombres, new JuegoId());
        var useCase = new CrearJuegoUseCase();


        Assertions.assertThrows(BusinessException.class, () -> {
            UseCaseHandler.getInstance()
                    .syncExecutor(useCase, new RequestCommand<>(command))
                    .orElseThrow();
        }, "No se puede crear el juego por que no tiene la cantidad necesaria de jugadores");

    }
}