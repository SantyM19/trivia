package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.command.RecibirRespuestas;
import com.example.trivia.domain.juego.events.JuegoCreado;
import com.example.trivia.domain.juego.events.JugadorAdicionado;
import com.example.trivia.domain.juego.events.RespuestasRecibidas;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;
import com.example.trivia.domain.juego.values.pregunta.Respuesta;
import com.example.trivia.domain.juego.values.pregunta.RespuestaId;
import com.example.trivia.domain.juego.values.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecibirRespuestasUseCaseTest {

    @Mock
    private DomainEventRepository repository;

    @Test
    void SolicitarRespuestas(){
        var id = JuegoId.of("0000");
        var jugadorId = JugadorId.of("xxxx");
        var respuestas = Map.of(
                RespuestaId.of("0001"), new Respuesta(true),
                RespuestaId.of("0002"), new Respuesta(false),
                RespuestaId.of("0003"), new Respuesta(true)
        );
        var preguntasId = Map.of(
                RespuestaId.of("0001"), PreguntaId.of("0001"),
                RespuestaId.of("0002"), PreguntaId.of("0002"),
                RespuestaId.of("0003"), PreguntaId.of("0003")
        );

        var command = new RecibirRespuestas(id,jugadorId,respuestas,preguntasId);
        var useCase = new RecibirRespuestasUseCase();

        when(repository.getEventsBy(id.value())).thenReturn(eventStored(id));
        useCase.addRepository(repository);

        var events = UseCaseHandler.getInstance()
                .setIdentifyExecutor(id.value())
                .syncExecutor(useCase, new RequestCommand<>(command))
                .orElseThrow()
                .getDomainEvents();

        var respuestaInicial = (RespuestasRecibidas) events.get(0);

        assertEquals("0000",respuestaInicial.getJuegoId().toString());
        assertEquals(3, respuestaInicial.getRespuestas().size());
        assertEquals(true ,respuestaInicial.getRespuestas().get(RespuestaId.of("0001")).getRespuesta().getRespuesta());
        assertEquals(false ,respuestaInicial.getRespuestas().get(RespuestaId.of("0002")).getRespuesta().getRespuesta());
        assertEquals(true ,respuestaInicial.getRespuestas().get(RespuestaId.of("0003")).getRespuesta().getRespuesta());

    }

    @Test
    void SolicitarRespuesta(){
        var id = JuegoId.of("0000");
        var jugadorId = JugadorId.of("xxxx");
        var respuestas = Map.of(
                RespuestaId.of("0001"), new Respuesta(true)
        );
        var preguntasId = Map.of(
                RespuestaId.of("0001"), PreguntaId.of("0001")
        );

        var command = new RecibirRespuestas(id,jugadorId,respuestas,preguntasId);
        var useCase = new RecibirRespuestasUseCase();

        when(repository.getEventsBy(id.value())).thenReturn(eventStored(id));
        useCase.addRepository(repository);

        var events = UseCaseHandler.getInstance()
                .setIdentifyExecutor(id.value())
                .syncExecutor(useCase, new RequestCommand<>(command))
                .orElseThrow()
                .getDomainEvents();

        var respuestaInicial = (RespuestasRecibidas) events.get(0);

        assertEquals("0000",respuestaInicial.getJuegoId().toString());
        assertEquals(1, respuestaInicial.getRespuestas().size());
        assertEquals(true ,respuestaInicial.getRespuestas().get(RespuestaId.of("0001")).getRespuesta().getRespuesta());

    }

    private List<DomainEvent> eventStored(JuegoId id) {
        return List.of(
                new JuegoCreado(id),
                new JugadorAdicionado(JugadorId.of("0001"), new Nombre("Chanfle"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0002"), new Nombre("Betelgeuse"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0003"), new Nombre("Rigel"), new Puntos(0), new Ganadas(0))
        );
    }

}