package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.Jugador;
import com.example.trivia.domain.juego.Pregunta;
import com.example.trivia.domain.juego.command.DarPuntos;
import com.example.trivia.domain.juego.events.JuegoCreado;
import com.example.trivia.domain.juego.events.JugadorAdicionado;
import com.example.trivia.domain.juego.events.PuntuacionesDadas;
import com.example.trivia.domain.juego.values.*;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;
import com.example.trivia.domain.juego.values.pregunta.Respuesta;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DarPuntosCaseUseTest {
    @Mock
    private DomainEventRepository repository;

    @Test
    void Puntuar(){
        var id = JuegoId.of("0000");
        var jugadorId = JugadorId.of("0001");
        var jugador = Map.of(
                jugadorId, new Jugador(jugadorId, new Nombre("Mee"), new Puntos(0), new Ganadas(1))
        );
        var preguntas = Map.of(
                PreguntaId.of("0001"), new Pregunta(PreguntaId.of("0001")," 2 x  2 = 4 ",new Respuesta(true)),
                PreguntaId.of("0002"), new Pregunta(PreguntaId.of("0002")," Germany is in Asia ",new Respuesta(false))
        );
        var respuestas = Map.of(
                PreguntaId.of("0001"), new Respuesta(true),
                PreguntaId.of("0002"), new Respuesta(true)
        );

        var command = new DarPuntos(id,jugador,preguntas,respuestas);
        var useCase = new DarPuntosCaseUse();

        when(repository.getEventsBy(id.value())).thenReturn(eventStored(id));
        useCase.addRepository(repository);

        var events = UseCaseHandler.getInstance()
                .setIdentifyExecutor(id.value())
                .syncExecutor(useCase, new RequestCommand<>(command))
                .orElseThrow()
                .getDomainEvents();

        var puntos = (PuntuacionesDadas) events.get(0);

        assertEquals("0000",puntos.getJuegoId().toString());

        assertEquals(1, puntos.getJugadorPuntos().size());
        assertEquals(new Puntos(1).value() ,puntos.getJugadorPuntos().get(JugadorId.of("0001")).puntos().value());

    }

    private List<DomainEvent> eventStored(JuegoId id) {
        return List.of(
                new JuegoCreado(id),
                new JugadorAdicionado(JugadorId.of("0001"), new Nombre("Chanfle"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0002"), new Nombre("Betelgeuse"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0003"), new Nombre("Rigel"), new Puntos(0), new Ganadas(0))
        );
    }
}