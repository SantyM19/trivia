package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.domain.generic.DomainEvent;


import static org.junit.jupiter.api.Assertions.*;

import com.example.trivia.domain.juego.command.InicializarJuego;
import com.example.trivia.domain.juego.events.JuegoCreado;
import com.example.trivia.domain.juego.events.JuegoInicializado;
import com.example.trivia.domain.juego.events.JugadorAdicionado;
import com.example.trivia.domain.juego.values.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InicializarJuegoUseCaseTest {

    @Mock
    private DomainEventRepository repository;

    @Test
    void inicializarJuego() {
        var id = JuegoId.of("0000");
        var command = new InicializarJuego(id);
        var useCase = new InicializarJuegoUseCase();

        when(repository.getEventsBy(id.value())).thenReturn(eventStored(id));
        useCase.addRepository(repository);

        var events = UseCaseHandler.getInstance()
                .setIdentifyExecutor(id.value())
                .syncExecutor(useCase, new RequestCommand<>(command))
                .orElseThrow()
                .getDomainEvents();

        var juegoInicializado = (JuegoInicializado) events.get(0);
        assertEquals(3, juegoInicializado.getJugadoresIds().size());

    }

    private List<DomainEvent> eventStored(JuegoId id) {
        return List.of(
                new JuegoCreado(id),
                new JugadorAdicionado(JugadorId.of("0001"), new Nombre("Chanfle"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0002"), new Nombre("Betelgeuse"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0003"), new Nombre("Rigel"), new Puntos(0), new Ganadas(0))
        );
    }
}