package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.Jugador;
import com.example.trivia.domain.juego.command.SeleccionarGanadores;
import com.example.trivia.domain.juego.events.GanadoresSeleccionados;
import com.example.trivia.domain.juego.events.JuegoCreado;
import com.example.trivia.domain.juego.events.JugadorAdicionado;
import com.example.trivia.domain.juego.values.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SeleccionarGanadoresUseCaseTest {
    @Mock
    private DomainEventRepository repository;

    @Test
    void Puntuar(){
        var id = JuegoId.of("0000");
        var jugador = Map.of(
                JugadorId.of("0001"), new Jugador(JugadorId.of("0001"), new Nombre("Mee"), new Puntos(5), new Ganadas(1)),
                JugadorId.of("0002"), new Jugador(JugadorId.of("0002"), new Nombre("Tuu"), new Puntos(1), new Ganadas(1)),
                JugadorId.of("0003"), new Jugador(JugadorId.of("0003"), new Nombre("Eel"), new Puntos(6), new Ganadas(1)),
                JugadorId.of("0004"), new Jugador(JugadorId.of("0004"), new Nombre("Ela"), new Puntos(3), new Ganadas(1))
        );


        var command = new SeleccionarGanadores(id,jugador);
        var useCase = new SeleccionarGanadoresUseCase();

        when(repository.getEventsBy(id.value())).thenReturn(eventStored(id));
        useCase.addRepository(repository);

        var events = UseCaseHandler.getInstance()
                .setIdentifyExecutor(id.value())
                .syncExecutor(useCase, new RequestCommand<>(command))
                .orElseThrow()
                .getDomainEvents();

        var ganador = (GanadoresSeleccionados) events.get(0);

        assertEquals("0000",ganador.getJuegoId().toString());

        assertEquals(4, ganador.getGanador().size());
        assertEquals(new Puntos(1).value() ,ganador.getGanador().get(JugadorId.of("0001")).ganadas().value());
        assertEquals(new Puntos(1).value() ,ganador.getGanador().get(JugadorId.of("0002")).ganadas().value());
        assertEquals(new Puntos(2).value() ,ganador.getGanador().get(JugadorId.of("0003")).ganadas().value());
        assertEquals(new Puntos(1).value() ,ganador.getGanador().get(JugadorId.of("0004")).ganadas().value());


    }

    private List<DomainEvent> eventStored(JuegoId id) {
        return List.of(
                new JuegoCreado(id),
                new JugadorAdicionado(JugadorId.of("0001"), new Nombre("Chanfle"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0002"), new Nombre("Betelgeuse"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0003"), new Nombre("Rigel"), new Puntos(0), new Ganadas(0))
        );
    }
}