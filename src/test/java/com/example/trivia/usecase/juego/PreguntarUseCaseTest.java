package com.example.trivia.usecase.juego;

import co.com.sofka.business.generic.UseCaseHandler;
import co.com.sofka.business.repository.DomainEventRepository;
import co.com.sofka.business.support.RequestCommand;
import co.com.sofka.domain.generic.DomainEvent;
import com.example.trivia.domain.juego.Pregunta;
import com.example.trivia.domain.juego.command.Preguntar;
import com.example.trivia.domain.juego.events.JuegoCreado;
import com.example.trivia.domain.juego.events.JugadorAdicionado;
import com.example.trivia.domain.juego.events.PreguntasRealizadas;
import com.example.trivia.domain.juego.values.*;
import com.example.trivia.domain.juego.values.pregunta.PreguntaId;
import com.example.trivia.domain.juego.values.pregunta.Respuesta;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PreguntarUseCaseTest {
    @Mock
    private DomainEventRepository repository;

    @Test
    void Preguntar(){
        var id = JuegoId.of("0000");
        var jugadorId = JugadorId.of("0001");
        var preguntas = Map.of(
                PreguntaId.of("0001"), new Pregunta(PreguntaId.of("0001")," 2 x  2 = 4 ",new Respuesta(true)),
                PreguntaId.of("0002"), new Pregunta(PreguntaId.of("0002")," Germany is in Asia ",new Respuesta(false))
        );

        var command = new Preguntar(id,jugadorId,preguntas);
        var useCase = new PreguntarUseCase();

        when(repository.getEventsBy(id.value())).thenReturn(eventStored(id));
        useCase.addRepository(repository);

        var events = UseCaseHandler.getInstance()
                .setIdentifyExecutor(id.value())
                .syncExecutor(useCase, new RequestCommand<>(command))
                .orElseThrow()
                .getDomainEvents();

        var preguntasR = (PreguntasRealizadas) events.get(0);

        assertEquals("0000",preguntasR.getJuegoId().toString());
        assertEquals(2, preguntasR.getPreguntas().size());
        assertEquals(true ,preguntasR.getPreguntas().get(PreguntaId.of("0001")).respuesta().getRespuesta());
        assertEquals(false ,preguntasR.getPreguntas().get(PreguntaId.of("0002")).respuesta().getRespuesta());
    }

    private List<DomainEvent> eventStored(JuegoId id) {
        return List.of(
                new JuegoCreado(id),
                new JugadorAdicionado(JugadorId.of("0001"), new Nombre("Chanfle"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0002"), new Nombre("Betelgeuse"), new Puntos(0), new Ganadas(0)),
                new JugadorAdicionado(JugadorId.of("0003"), new Nombre("Rigel"), new Puntos(0), new Ganadas(0))
        );
    }
}